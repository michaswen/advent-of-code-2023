def IsSymbol(character):
    if not character.isnumeric() and character != "." and character != "\n":
        return True
    return False


def FindNumberLeft(line, startindex):
    currentchar = line[startindex]
    if currentchar.isnumeric():
        return FindNumberLeft(line, startindex - 1) + currentchar
    return ""


def FindNumberRight(line, startindex):
    currentchar = line[startindex]
    if currentchar.isnumeric():
        return currentchar + FindNumberRight(line, startindex + 1)
    return ""


def FindNumberLeftAndRight(line, startindex):
    currentchar = line[startindex]
    if currentchar.isnumeric():
        return FindNumberLeft(line, startindex) + FindNumberRight(line, startindex + 1)
    return ""

def part1():
    data = open("day3data")
    partnumbers = []
    prevprevline = ""
    prevline = ""
    for line in data:
        for i in range(len(prevline)):
            char = prevline[i]
            if IsSymbol(char):
                #same line recognition
                partnumbers.append(FindNumberLeft(prevline, i-1))
                partnumbers.append(FindNumberRight(prevline, i + 1))

                #above line recognition
                abovenumbers = set()
                abovenumbers.add(FindNumberLeftAndRight(prevprevline, i-1))
                abovenumbers.add(FindNumberLeftAndRight(prevprevline, i))
                abovenumbers.add(FindNumberLeftAndRight(prevprevline, i+1))
                partnumbers += list(abovenumbers)

                #below line recognition
                belownumbers = set()
                belownumbers.add(FindNumberLeftAndRight(line, i-1))
                belownumbers.add(FindNumberLeftAndRight(line, i))
                belownumbers.add(FindNumberLeftAndRight(line, i+1))
                partnumbers += list(belownumbers)

        prevprevline = prevline
        prevline = line
    partnumbers = [int(i) for i in partnumbers if i]
    return sum(partnumbers)


def part2():
    data = open("day3data")
    partnumbers = []
    prevprevline = ""
    prevline = ""
    for line in data:
        for i in range(len(prevline)):
            char = prevline[i]
            if char == "*":
                partnumbersthisgear = []

                # same line recognition
                partnumbersthisgear.append(FindNumberLeft(prevline, i - 1))
                partnumbersthisgear.append(FindNumberRight(prevline, i + 1))

                # above line recognition
                abovenumbers = set()
                abovenumbers.add(FindNumberLeftAndRight(prevprevline, i - 1))
                abovenumbers.add(FindNumberLeftAndRight(prevprevline, i))
                abovenumbers.add(FindNumberLeftAndRight(prevprevline, i + 1))
                partnumbersthisgear += list(abovenumbers)

                # below line recognition
                belownumbers = set()
                belownumbers.add(FindNumberLeftAndRight(line, i - 1))
                belownumbers.add(FindNumberLeftAndRight(line, i))
                belownumbers.add(FindNumberLeftAndRight(line, i + 1))
                partnumbersthisgear += list(belownumbers)

                partnumbersthisgear = [int(i) for i in partnumbersthisgear if i]
                if len(partnumbersthisgear) == 2:
                    partnumbers.append(partnumbersthisgear[0] * partnumbersthisgear[1])

        prevprevline = prevline
        prevline = line
    return sum(partnumbers)

print(part1())
print(part2())