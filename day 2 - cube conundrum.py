

def part_1():
    data = open("day2data")
    # red, green, blue
    maxcubes = [12, 13, 14]
    gameid = 0
    gameidsum = 0
    for line in data:
        gameid += 1
        allgames = line.split(":")[1:]
        totalthrows = [i.split(";") for i in allgames].pop()
        faultygame = False
        for indivthrows in totalthrows:
            indivthrows = indivthrows.split(",")
            indivthrows = [i.strip() for i in indivthrows]
            print(indivthrows)
            for throw in indivthrows:
                nr = int(throw.split(" ")[0])
                if "red" in throw:
                    if nr > maxcubes[0]:
                        faultygame = True
                elif "green" in throw:
                    if nr > maxcubes[1]:
                        faultygame = True
                elif "blue" in throw:
                    if nr > maxcubes[2]:
                        faultygame = True
        if not faultygame:
            gameidsum += gameid
    print(gameidsum)


def part_2():
    data = open("day2data")
    gamepowersum = 0
    #red, green, blue
    for line in data:
        gamepower = 0
        mincubes = [0, 0, 0]
        allgames = line.split(":")[1:]
        totalthrows = [i.split(";") for i in allgames].pop()
        faultygame = False
        for indivthrows in totalthrows:
            indivthrows = indivthrows.split(",")
            indivthrows = [i.strip() for i in indivthrows]
            print(indivthrows)
            for throw in indivthrows:
                nr = int(throw.split(" ")[0])
                if "red" in throw:
                    if nr > mincubes[0]:
                        mincubes[0] = nr
                elif "green" in throw:
                    if nr > mincubes[1]:
                        mincubes[1] = nr
                elif "blue" in throw:
                    if nr > mincubes[2]:
                        mincubes[2] = nr
        gamepower = mincubes[0] * mincubes[1] * mincubes[2]
        gamepowersum += gamepower
        print(mincubes)
        print(gamepowersum)


part_1()
part_2()