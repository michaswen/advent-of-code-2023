fin = open("day1trebuchetdata")
calsum = 0
nmbrs = ["one", "two", "three", "four", "five", "six", "seven", "eight", "nine"]
for i in fin:
    l = [0 for y in (range(len(i)))]

    for k in range(9):
        nr = nmbrs[k]
        if nr in i:
            cnt = i.count(nr)
            buffer = 0
            for o in range(cnt):
                ind = i.find(nr, buffer)
                l[ind] = k+1
                buffer = ind + len(nr)

    for j in range(len(i)):
        if i[j].isnumeric():
            l[j] = int(i[j])

    l = [i for i in l if i != 0]

    result = int(str(l[0]) + str(l[-1]))
    calsum += result
print(calsum)
