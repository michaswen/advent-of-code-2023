def part1():
    data = open("day4data")
    totalscore = 0
    for line in data:
        cardnr, allnrs = line.split(":")
        winning, my = allnrs.split("|")
        winning = winning.split(" ")
        winning = [int(i) for i in winning if i]
        my = my.split(" ")
        my = [int(i) for i in my if i]

        score = 0
        for mynr in my:
            if mynr in winning:
                if score == 0:
                    score = 1
                else:
                    score *= 2

        totalscore += score
    return totalscore


def updatebackloglist(backloglist, wincount):
    for i in range(wincount):
        if len(backloglist) < i+1:
            backloglist.append(1)
        else:
            backloglist[i] += 1
    return backloglist


def part2():
    data = open("day4data")
    totalcards = 0
    backloglist = []
    for line in data:
        cardnr, allnrs = line.split(":")
        winning, my = allnrs.split("|")
        winning = winning.split(" ")
        winning = [int(i) for i in winning if i]
        my = my.split(" ")
        my = [int(i) for i in my if i]

        totalcards += 1
        wincount = 0
        for mynr in my:
            if mynr in winning:
                wincount += 1

        if backloglist:
            extracurrentcard = backloglist[0]
            totalcards += backloglist.pop(0)
        else:
            extracurrentcard = 0

        if wincount > 0:
            for i in range(1 + extracurrentcard):
                backloglist = updatebackloglist(backloglist, wincount)

    return totalcards


print(part1())
print(part2())